# Firefox configuration

> Preferences can be set at startup using the `user.js` file.
> This file doesn't exist by default.
> https://support.mozilla.org/en-US/questions/965842


## This role uses the following Git submodules

This repo contains submodules, so you should always clone it using `git clone --recursive`.

+ `firefox-browser/files/user.js -> git@github.com:solarchemist/user.js.git`
+ `firefox-browser/files/theme-lepton -> git@github.com:solarchemist/Firefox-UI-Fix.git`

`github.com/solarchemist/user.js` is my fork of https://github.com/pyllyukko/user.js.
`git@github.com:solarchemist/Firefox-UI-Fix.git` is my fork of https://github.com/black7375/Firefox-UI-Fix.


## Firefox hardening

This role sets a custom `user.js`.


## Firefox themes

+ https://github.com/black7375/Firefox-UI-Fix (resets keys in `user.js`, defines `userChrome.css` and `userContent.css`)
+ https://github.com/leadweedy/Firefox-Proton-Square (fork of `black7375/Firefox-UI-Fix`, defines `userChrome.css` and `userContent.css`)
+ https://piunikaweb.com/2021/08/16/here-are-some-workarounds-to-disable-proton-ui-fix-tabs-in-firefox-91/
+ https://reddit.com/r/firefox/comments/3gz8by/square_tabs_why_do_no_themes_do_it_right/
+ https://git.sr.ht/~ranmaru/ff-vertical-tabs
+ https://github.com/wilfredwee/photon-australis
+ https://github.com/pratyushtewari/firefox-like-chrome

## Firefox extensions

Extensions are handled manually.
Thanks to Firefox Sync, extensions and their settings are synced across my different Firefox instances.

Extensions I can recommend:

+ [Awesome RSS](https://shgysk8zer0.github.io/)
+ [PassFF](https://github.com/passff/passff)
+ [Fraidycat](https://fraidyc.at/)
+ [I still don't care about cookies](https://github.com/OhMyGuus/I-Still-Dont-Care-About-Cookies)
+ [Privacy Redirect](https://github.com/SimonBrazell/privacy-redirect)
+ [Wallabagger](https://github.com/wallabag/wallabagger)
+ [Web Extension for Shaarli](https://github.com/ikipatang/shaarli-web-extension)
+ [Zotero Connector](https://www.zotero.org/download/connectors)
+ [Firefox Translations](https://github.com/mozilla/firefox-translations)


Awesome RSS [stopped working with TinyTinyRSS](https://github.com/shgysk8zer0/awesome-rss/issues/144).
[hockeymike@github.com suggested a simple fix](https://github.com/shgysk8zer0/awesome-rss/pull/165),
which I wanted to try to see if it indeed fixes the problem.

Ignorant as I was about Firefox extension packaging, loading a modified extension
took me a while to figure out. For next time, here's how to:

+ downloaded the original (broken) Firefox extension ZIP file from the Github release page
  https://github.com/shgysk8zer0/awesome-rss/releases
+ unpackaged the zip file `unzip awesome-rss.zip`
+ edited the two lines in question
  https://github.com/shgysk8zer0/awesome-rss/pull/165/files
+ repackaged into ZIP (very important to use `-r` flag, otherwise missing files
  inside directories!)
  `zip -r awesome-rss-1.3.6.zip *`
+ This ZIP file is refused by Firefox unless you first set
  `xpinstall.signatures.required = false` in `about:config`.

I have placed a copy of this modified extension in `./files/`.
Let's see how it goes.


### Firefox extensions on Android

Not straight-forward at all, but possible to make it work using Fennec (~~but not Firefox~~).

+ https://reddit.com/r/fossdroid/comments/p7mgec/guide_using_most_all_firefox_addons_on_fennec_on
+ https://blog.mozilla.org/en/mozilla/new-extensions-youll-love-now-available-on-firefox-for-android

Firefox beta on Android recently gained the ability to use custom add-on collections:  
https://www.ghacks.net/2022/10/20/firefox-beta-for-android-now-supports-custom-add-on-collections



## Some ways to test your browser's "uniqueness"

Ideally, you should be very non-unique.

+ https://panopticlick.eff.org
+ https://browserleaks.com/
+ https://amiunique.org/



## Links and notes

+ https://www.andreafortuna.org/2017/06/14/firefox-configuration-hardening-using-a-single-file
+ https://github.com/pyllyukko/user.js
+ http://user.it.uu.se/~arvge836/cryptoparty/191122-Firefox-Hardening.pdf
+ https://restoreprivacy.com/firefox-privacy
+ https://gist.github.com/0XDE57/fbd302cef7693e62c769
+ https://community.tt-rss.org/t/firefox-integration-button-will-stop-working-in-firefox-59/865
+ https://ryandaniels.ca/blog/firefox-i-love-you-but-youre-bringing-me-down
+ https://dataswamp.org/~solene/2023-09-24-harden-firefox-with-arkenfox.html
+ https://hacks.mozilla.org/2023/11/firefox-developer-edition-and-beta-try-out-mozillas-deb-package
+ https://vermaden.wordpress.com/2024/03/18/sensible-firefox-setup
