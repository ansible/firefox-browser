# Firefox extensions

Just to be clear, this role does not install or manage Firefox plugins.


## My extensions

+ Awesome RSS
+ EZProxy Redirect Foxified (consider deprecating now that university affiliation ended)
+ Firefox Translations
+ Fraidycat
+ I still don't care about cookies
+ PassFF
+ Privacy Redirect
+ Wallabagger
+ Web Extension for Shaarli


## Extensions under consideration

+ [Saka Key](https://key.saka.io) - browse using your keyboard
+ [Tridactyl](https://github.com/tridactyl/tridactyl) - browse using your keyboard
+ ArchiveBox Browser Extension (NOT WORKING! I think because extension does not auth, assumes open archivebox)


## Links and notes

+ https://github.com/ArchiveBox/archivebox-browser-extension
+ https://github.com/layderv/archivefox (not tested this one)
